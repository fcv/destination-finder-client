package com.afkl.cases.df.fare;

import com.afkl.cases.df.airport.Airport;

import java.io.Serializable;
import java.math.BigDecimal;

public class FareWithAirports extends AbstractFare implements Serializable {

    private static final long serialVersionUID = 4550698483513047972L;

    public Airport origin;
    public Airport destination;

    public FareWithAirports() {
    }

    public FareWithAirports(Fare fare, Airport origin, Airport destination) {
        super(fare.getAmount(), fare.getCurrency());
        this.origin = origin;
        this.destination = destination;
    }

    public FareWithAirports(BigDecimal amount, String currency, Airport origin, Airport destination) {
        super(amount, currency);
        this.origin = origin;
        this.destination = destination;
    }

    public Airport getOrigin() {
        return origin;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FareWithAirports{");
        sb.append("amount=").append(getAmount());
        sb.append(", currency=").append(getCurrency());
        sb.append(", origin=").append(origin);
        sb.append(", destination=").append(destination);
        sb.append('}');
        return sb.toString();
    }

}
