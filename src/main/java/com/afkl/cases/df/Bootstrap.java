package com.afkl.cases.df;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.io.IOException;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@SpringBootApplication
public class Bootstrap {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Bootstrap.class, args);
    }

    @Bean
    @Scope(value = SCOPE_PROTOTYPE)
    public Logger getLogger(InjectionPoint ip) {
        Class<?> loggerClass = ip.getMember().getDeclaringClass();
        return LoggerFactory.getLogger(loggerClass);
    }

}
