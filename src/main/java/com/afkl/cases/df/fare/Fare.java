package com.afkl.cases.df.fare;

import java.io.Serializable;
import java.math.BigDecimal;

public class Fare extends AbstractFare implements Serializable {

    private String origin;
    private String destination;

    public Fare() {
    }

    public Fare(BigDecimal amount, String currency, String origin, String destination) {
        super(amount, currency);
        this.origin = origin;
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Fare{");
        sb.append("amount=").append(getAmount());
        sb.append(", currency=").append(getCurrency());
        sb.append(", origin=").append(origin);
        sb.append(", destination=").append(destination);
        sb.append('}');
        return sb.toString();
    }
}
