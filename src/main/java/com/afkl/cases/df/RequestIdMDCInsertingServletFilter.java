package com.afkl.cases.df;

import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.UUID.randomUUID;

/**
 * Inserts a unique generated values to {@link MDC} under {@code "req.id"} name.
 *
 * Implementation based on Logback's {@link ch.qos.logback.classic.helpers.MDCInsertingServletFilter}
 */
public class RequestIdMDCInsertingServletFilter implements Filter {

    private static final String REQUEST_ID = "req.id";

    private AtomicLong sequence = new AtomicLong(0);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        insertIntoMDC(request);
        try {
            chain.doFilter(request, response);
        } finally {
            clearMDC();
        }
    }

    protected void insertIntoMDC(ServletRequest request) {
        generateRequestId(request).ifPresent(id -> {
            MDC.put(REQUEST_ID, id);
        });
    }

    protected Optional<String> generateRequestId(ServletRequest request) {
        return Optional.of('#' + String.valueOf(sequence.incrementAndGet()));
    }

    protected void clearMDC() {
        MDC.remove(REQUEST_ID);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
