package com.afkl.cases.df.fare;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@ConfigurationProperties(prefix = "destination-finder.remote-api.fare")
public class FareApiProperties {

    @NotNull
    private String faresUrlTemplate;

    public FareApiProperties() {
    }

    public FareApiProperties(String faresUrlTemplate) {
        this.faresUrlTemplate = faresUrlTemplate;
    }

    public String getFaresUrlTemplate() {
        return faresUrlTemplate;
    }

    public void setFaresUrlTemplate(String faresUrlTemplate) {
        this.faresUrlTemplate = faresUrlTemplate;
    }
}
