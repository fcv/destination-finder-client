package com.afkl.cases.df.fare;

import com.afkl.cases.df.airport.Airport;
import com.afkl.cases.df.airport.AirportService;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static com.afkl.cases.df.fare.RemoteFareService.Route.route;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class RemoteFareService implements FareService {

    private final Logger logger;
    private final RestTemplate restTemplate;
    private final FareApiProperties apiProperties;
    private final AirportService airportService;
    private final Executor executor;

    public RemoteFareService(Logger logger, RestTemplate restTemplate, FareApiProperties apiProperties, AirportService airportService, Executor executor) {
        this.logger = requireNonNull(logger, "logger cannot be null");
        this.restTemplate = requireNonNull(restTemplate, "restTemplate cannot be null");
        this.apiProperties = requireNonNull(apiProperties, "apiProperties cannot be null");
        this.airportService = requireNonNull(airportService, "airportService cannot be null");
        this.executor = requireNonNull(executor, "executor cannot be null");
    }

    @Override
    public Optional<Fare> getFare(String originCode, String destinationCode, Optional<String> currency) {
        logger.debug("getFare(originCode: {}, destinationCode: {}, currency: {})", originCode, destinationCode, currency);

        String urlTemplate = apiProperties.getFaresUrlTemplate();
        UriComponentsBuilder uriBuilder = fromHttpUrl(urlTemplate);
        currency.ifPresent(value -> uriBuilder.queryParam("currency", value));
        URI uri = uriBuilder.buildAndExpand( originCode, destinationCode).encode().toUri();

        Fare fare = restTemplate.getForObject(uri, Fare.class);
        return Optional.of(fare);
    }

    @Override
    public CompletableFuture<Optional<FareWithAirports>> getFareWithAirports(String originCode, String destinationCode, Optional<String> currency) {
        logger.debug("getFareWithAirports(originCode: {}, destinationCode: {}, currency: {})", originCode, destinationCode, currency);

        CompletableFuture<Optional<Airport>> originAirportStage = supplyAsync(
                () -> airportService.getAirportByCode(originCode), executor);
        CompletableFuture<Optional<Airport>> destinationAirportStage = supplyAsync(
                () -> airportService.getAirportByCode(destinationCode), executor);
        CompletableFuture<Optional<Fare>> fareStage = supplyAsync(() -> getFare(originCode, destinationCode, currency), executor);

        return originAirportStage
                .thenCombineAsync(destinationAirportStage, (optionalOrigin, optionalDestination) -> {
                    logger.debug("then combine (optionalOrigin: {}, optionalDestination: {})", optionalOrigin, optionalDestination);
                    return optionalOrigin
                            .flatMap(origin ->
                                    optionalDestination.map(destination ->
                                            route(origin, destination)));
                }, executor)
                .thenCombineAsync(fareStage, (optionalRoute, optionalFare) -> {
                    logger.debug("then combine (optionalRoute: {}, optionalFare: {})", optionalRoute, optionalFare);
                    return optionalFare.flatMap(fare -> optionalRoute.map(route -> fareWithAirports(fare, route)));
                }, executor);
    }

    protected final FareWithAirports fareWithAirports(Fare fare, Route<Airport> route) {
        return new FareWithAirports(fare, route.getOrigin(), route.getDestination());
    }

    public static class Route<T> {
        private final T origin;
        private final T destination;

        private Route(T origin, T destination) {
            this.origin = origin;
            this.destination = destination;
        }

        public T getOrigin() {
            return origin;
        }

        public T getDestination() {
            return destination;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Route{");
            sb.append("origin=").append(origin);
            sb.append(", destination=").append(destination);
            sb.append('}');
            return sb.toString();
        }

        public static final <T, P extends T> Route<T> route(P origin, P destination) {
            return new Route<T>(origin, destination);
        }

    }
}
