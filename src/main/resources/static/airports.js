$(function($) {

    var $mainPanel = $('body'),
        $airportsTable = $('.airports-table', $mainPanel);

    function renderAirports(airports) {

        var template = $('#airport-rows-tmpl').html();
        Mustache.parse(template);
        var renderedHtml = Mustache.render(template, airports);
        $('tbody', $airportsTable).html(renderedHtml);
    }

    function renderPageMetadata(pageMetadata) {

        // safety copy
        pageMetadata = $.extend({}, pageMetadata);
        // page's number is 0-indexed this increment 1 before showing in UI
        pageMetadata.number = pageMetadata.number + 1;
        var template = $('#page-metadata-tmpl').html();
        Mustache.parse(template);
        var renderedHtml = Mustache.render(template, pageMetadata);
        $('thead .page-metadata-panel', $airportsTable).html(renderedHtml);
    }

    function renderNavigationLink(navigationLinks) {
        var template = $('#navigation-links-tmpl').html();
        Mustache.parse(template);
        var renderedHtml = Mustache.render(template, navigationLinks);
        $('thead .navigation-links-panel', $airportsTable).html(renderedHtml);
    }

    function load(options) {

        var $tbody = $('tbody', $airportsTable);
        options = $.extend({}, {
            endpointUrl: $tbody.data('endpoint-url')
        }, options);

        var endpointUrl = options.endpointUrl;

        $tbody.empty();
        $airportsTable.addClass('loading');

        var promise = $.ajax({
            url: endpointUrl,
            method: 'GET'
        });

        promise
            .done(function(response) {

                var airports = response._embedded.airports,
                    page = response.page,
                    links = response._links;

                renderAirports(airports);
                renderPageMetadata(page);
                renderNavigationLink(links);
            })
            .always(function() {
                $airportsTable.removeClass('loading');
            });

    };

    function init() {

        load();
        $airportsTable.on('click', '.navigation-links-panel a:not(disabled)', function(evt) {

            evt.preventDefault();
            var href = $(this).attr('href');
            load({
                endpointUrl: href
            });
        });
    }

    init();
});
