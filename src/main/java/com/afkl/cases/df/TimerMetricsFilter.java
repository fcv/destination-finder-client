package com.afkl.cases.df;

import com.google.common.collect.ImmutableSet;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

/**
 * Servlet Filter implementation that collects information about elapsed time with a request and register its
 * under 'timer.' + request's path key.
 *
 * Based on Spring's MetricsFilter which could not be extends since most of its methods are private.
 */
@Order(HIGHEST_PRECEDENCE)
public class TimerMetricsFilter implements Filter {

    private static final Set<PatternReplacer> KEY_REPLACERS = ImmutableSet.<PatternReplacer>builder()
            .add(new PatternReplacer("/", Pattern.LITERAL, "."))
            .add(new PatternReplacer("..", Pattern.LITERAL, "."))
            .build();

    private final GaugeService gaugeService;

    public TimerMetricsFilter(GaugeService gaugeService) {
        this.gaugeService = gaugeService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            long start = System.nanoTime();
            try {

                chain.doFilter(request, response);
            } finally {
                if (!httpRequest.isAsyncStarted()) {

                    long end = System.nanoTime();
                    String path = new UrlPathHelper().getPathWithinApplication(httpRequest);
                    String key = getKey(path);
                    gaugeService.submit("timer" + key, end - start);
                }
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    // See MetricsFilter#getKey
    private String getKey(String string) {

        String key = string;
        for (PatternReplacer replacer : KEY_REPLACERS) {
            key = replacer.apply(key);
        }
        if (key.endsWith(".")) {
            key = key + "root";
        }
        if (key.startsWith("_")) {
            key = key.substring(1);
        }
        return key;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    private static class PatternReplacer {

        private final Pattern pattern;

        private final String replacement;

        PatternReplacer(String regex, int flags, String replacement) {
            this.pattern = Pattern.compile(regex, flags);
            this.replacement = replacement;
        }

        public String apply(String input) {
            return this.pattern.matcher(input)
                    .replaceAll(Matcher.quoteReplacement(this.replacement));
        }

    }
}
