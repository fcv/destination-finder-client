package com.afkl.cases.df.airport;

import org.springframework.data.domain.Page;

import java.util.Optional;

public interface AirportService {

    Page<Airport> listAirports(Optional<Integer> size, Optional<Integer> page, Optional<String> lang, Optional<String> term);

    Optional<Airport> getAirportByCode(String code);

}
