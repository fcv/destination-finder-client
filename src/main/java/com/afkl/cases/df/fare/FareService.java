package com.afkl.cases.df.fare;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface FareService {

    public Optional<Fare> getFare(String originCode, String destinationCode, Optional<String> currency);

    public CompletableFuture<Optional<FareWithAirports>> getFareWithAirports(String originCode, String destinationCode, Optional<String> currency);
}
