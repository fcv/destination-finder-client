package com.afkl.cases.df.airport;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class AirportResourceAssembler extends ResourceAssemblerSupport<Airport, Resource<Airport>> {

    private static final Class<AirportEndpointController> controllerType = AirportEndpointController.class;

    public AirportResourceAssembler() {
        super(controllerType, resourceType());
    }

    @Override
    public Resource<Airport> toResource(Airport airport) {
        Link link = linkTo(methodOn(controllerType).getAirport(airport.getCode())).withSelfRel();
        return new Resource<Airport>(airport, link);
    }

    public static Class<Resource<Airport>> resourceType() {
        Class<?> genericType = Resource.class;
        @SuppressWarnings("unchecked")
        Class<Resource<Airport>> resourceType = (Class<Resource<Airport>>) genericType;
        return resourceType;
    }
}
