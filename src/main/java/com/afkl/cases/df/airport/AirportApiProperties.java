package com.afkl.cases.df.airport;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@ConfigurationProperties(prefix = "destination-finder.remote-api.airport")
public class AirportApiProperties {

    @NotNull
    private String airportsUrlTemplate;

    @NotNull
    private String airportUrlTemplate;

    public AirportApiProperties() {
    }

    public AirportApiProperties(String airportsUrlTemplate, String airportUrlTemplate) {
        this.airportsUrlTemplate = airportsUrlTemplate;
        this.airportUrlTemplate = airportUrlTemplate;
    }

    public String getAirportsUrlTemplate() {
        return airportsUrlTemplate;
    }

    public void setAirportsUrlTemplate(String airportsUrlTemplate) {
        this.airportsUrlTemplate = airportsUrlTemplate;
    }

    public String getAirportUrlTemplate() {
        return airportUrlTemplate;
    }

    public void setAirportUrlTemplate(String airportUrlTemplate) {
        this.airportUrlTemplate = airportUrlTemplate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AirportApiProperties{");
        sb.append("airportsUrlTemplate='").append(airportsUrlTemplate).append('\'');
        sb.append(", airportUrlTemplate='").append(airportUrlTemplate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
