$(function($) {

    var $mainPanel = $('body'),
        DEFAULT_CURRENCY = 'EUR';

    function loadFares(originCode, destinationCode, currency) {
        var $form = $('form', $mainPanel),
            $fareContainer = $('.fare-container', $mainPanel),
            endpointUrlTemplate = $form.attr('action'),
            endpointUrl = endpointUrlTemplate.replace('{originCode}', originCode).replace('{destinationCode}', destinationCode),
            method = $form.attr('method'),
            currency = currency || DEFAULT_CURRENCY
            ;

        $fareContainer.addClass('loading');
        var farePromise = $.ajax({
            url: endpointUrl,
            method: method,
            data: {
                currency: currency
            }
        });

        farePromise
            .done(function(fare) {

                var template = $('#fare-tmpl', $mainPanel).html();
                Mustache.parse(template);
                var renderedHtml = Mustache.render(template, fare);
                $('.fare-panel-container', $fareContainer).html(renderedHtml);
            })
            .always(function() {
                $fareContainer.removeClass('loading');
            })
            ;
    };

    function registerAutocomplete() {
        var cache = {};
        $('[name=origin], [name=destination]', $mainPanel)
            .autocomplete({
                source: function(request, response) {

                    var term = request.term;
                    if ( term in cache ) {
                        var cachedValue = cache[term];
                        response(cachedValue);
                        return;
                    }

                    var endpointUrl = this.element.data('endpoint-url'),
                        method = 'GET';

                    var airportsPromise = $.ajax({
                        url: endpointUrl,
                        method: method,
                        data: {
                            term: term
                        }
                    });

                    airportsPromise.done(function(airports) {
                        if (airports._embedded && airports._embedded.airports) {
                            airports = airports._embedded.airports;
                        }
                        var items = airports.map(function(airport) {
                            return {
                                value: airport.code,
                                label: '(' + airport.code + ') ' + airport.name

                            };
                        });
                        cache[term] = items;
                        response(items);
                    });

                }
            })
            .on('autocompleteselect', function(event, ui) {

                var selectedItem = ui.item;
                // NOTE: holding selected value as a data attribute because it might differ
                // from input's value
                $(this).data('value', ui.item.value);
            })
            .blur(function(evt) {

                // return input's value to previously selected one just in case user changed input's text but
                // didn't actually selected any value
                var selectedValue = $(this).data('value');
                $(this).val(selectedValue);
            });
    };

    function registerChangeHandler() {

        var changeHandler = function() {

            var originCode = $('[name=origin]', $mainPanel).data('value');
            var destinationCode = $('[name=destination]', $mainPanel).data('value');
            var currency = $('[name=currency]', $mainPanel).val();

            if (originCode && destinationCode) {
                loadFares(originCode, destinationCode, currency);
            }
        }

        $('[name=origin], [name=destination]', $mainPanel).on('autocompleteselect', changeHandler);
        $('[name=currency]', $mainPanel).change(changeHandler);
    };

    function init() {

        registerAutocomplete();
        registerChangeHandler();
    };

    init();

});
