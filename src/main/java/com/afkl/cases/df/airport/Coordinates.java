package com.afkl.cases.df.airport;

import java.io.Serializable;

import static java.lang.String.format;

public class Coordinates implements Serializable {

    private static final long serialVersionUID = 7830864208466773355L;

    /**
     * Maximum value for latitude
     */
    public static final double MAX_LAT = 90;

    /**
     * Minimum value for latitude
     */
    public static final double MIN_LAT = -90;

    /**
     * Maximum value for longitude
     */
    public static final double MAX_LNG = 180;

    /**
     * Minimum value for longitude
     */
    public static final double MIN_LNG = -180;

    public double latitude;
    public double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = checkLatitudeValue(latitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = checkLongitudeValue(longitude);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Coordinates{");
        sb.append("latitude=").append(latitude);
        sb.append(", longitude=").append(longitude);
        sb.append('}');
        return sb.toString();
    }

    private static double checkLatitudeValue(double latitude) {
        if (latitude > MAX_LAT || latitude < MIN_LAT) {
            throw new IllegalArgumentException(format("latitude %s is out of bounds [%s, %s]", latitude, MIN_LAT, MAX_LAT));
        }
        return latitude;
    }

    private static double checkLongitudeValue(double longitude) {
        if (longitude > MAX_LNG || longitude < MIN_LNG) {
            throw new IllegalArgumentException(format("longitude %s is out of bounds [%s, %s]", longitude, MIN_LNG, MAX_LNG));
        }
        return longitude;
    }

}
