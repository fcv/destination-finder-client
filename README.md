Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/instructions.html](http://localhost:9000/travel/instructions.html)

# Development

UI is available under root path: [http://localhost:9000/travel/](http://localhost:9000/travel/).

System Administration, with metrics and gauges , is available under `/admin/` path: [http://localhost:9000/travel/admin](http://localhost:9000/travel/admin).