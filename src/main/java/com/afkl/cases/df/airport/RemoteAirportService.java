package com.afkl.cases.df.airport;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.PagedResources.PageMetadata;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.Math.toIntExact;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class RemoteAirportService implements AirportService {

    private final Logger logger;
    private final RestTemplate restTemplate;
    private final AirportApiProperties apiProperties;

    public RemoteAirportService(Logger logger, RestTemplate restTemplate, AirportApiProperties apiProperties) {
        this.logger = logger;
        this.restTemplate = restTemplate;
        this.apiProperties = apiProperties;
    }

    @Override
    public Page<Airport> listAirports(Optional<Integer> size, Optional<Integer> page, Optional<String> lang, Optional<String> term) {

        logger.debug("listAirports(size: {}, page: {}, lang: {}, term: {})", size, page, lang, term);
        String urlTemplate = apiProperties.getAirportsUrlTemplate();

        UriComponentsBuilder uriBuilder = fromHttpUrl(urlTemplate);
        Map<String, Optional<?>> supportedParameters = ImmutableMap.<String, Optional<?>>builder()
                .put("size", size)
                .put("page", page)
                .put("lang", lang)
                .put("term", term)
                .build();

        supportedParameters.entrySet().stream().forEach(entry -> {
            entry.getValue().ifPresent(parameterValue -> {
                String parameterName = entry.getKey();
                uriBuilder.queryParam(parameterName, parameterValue);
            });
        });

        String url = uriBuilder.toUriString();
        ParameterizedTypeReference<PagedResources<Resource<Airport>>> responseType;
        responseType = new ParameterizedTypeReference<PagedResources<Resource<Airport>>>() {
        };
        RequestEntity<?> requestEntity = null;
        ResponseEntity<PagedResources<Resource<Airport>>> responseEntity;

        final Page<Airport> airportPage;
        responseEntity = restTemplate.exchange(url, GET, requestEntity, responseType);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.equals(OK)) {

            airportPage = newPage(responseEntity.getBody());
        } else if (statusCode.equals(HttpStatus.NOT_FOUND)) {
            airportPage = emptyPage();
        } else {
            logger.warn("Request to '{}' resulted in an unexpected status code {}, falling back to empty page", url, statusCode);
            airportPage = emptyPage();
        }
        return airportPage;
    }

    private Page<Airport> emptyPage() {
        return new PageImpl<Airport>(emptyList());
    }

    private Page<Airport> newPage(PagedResources<Resource<Airport>> pagedResource) {
        PageMetadata pageMetadata = pagedResource.getMetadata();

        int numberOfPage = toIntExact(pageMetadata.getNumber());
        int sizeOfPage = toIntExact(pageMetadata.getSize());
        int totalNumberOfRecords = toIntExact(pageMetadata.getTotalElements());

        Pageable pageable = new PageRequest(numberOfPage, sizeOfPage);
        List<Airport> a = pagedResource.getContent().stream()
                .map(resource -> resource.getContent()).collect(toList());

        return new PageImpl<Airport>(a, pageable, totalNumberOfRecords);
    }

    @Override
    public Optional<Airport> getAirportByCode(String code) {
        logger.debug("getAirportByCode(code: {})", code);
        String urlTemplate = apiProperties.getAirportUrlTemplate();
        String url =  fromHttpUrl(urlTemplate)
                .buildAndExpand(code)
                .toUriString();

        ResponseEntity<Airport> responseEntity = restTemplate.getForEntity(url, Airport.class);
        HttpStatus statusCode = responseEntity.getStatusCode();

        final Optional<Airport> airport;
        if (statusCode.equals(OK)) {
            airport = Optional.of(responseEntity.getBody());
        } else if (statusCode.equals(HttpStatus.NOT_FOUND)) {
            airport = Optional.empty();
        } else {
            logger.warn("Request to '{}' resulted in an unexpected status code {}, falling back to empty optional", url, statusCode);
            airport = Optional.empty();
        }
        return airport;
    }
}
