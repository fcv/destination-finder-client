package com.afkl.cases.df.fare;

import com.afkl.cases.df.airport.AirportService;
import org.slf4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static org.springframework.http.HttpStatus.GATEWAY_TIMEOUT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(value = "/api/rest/v1/fares", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FareEndpointController {

    private final Logger logger;
    private final FareService fareService;
    private final AirportService airportService;

    public FareEndpointController(Logger logger, FareService fareService, AirportService airportService) {
        this.logger = requireNonNull(logger, "logger cannot be null");
        this.fareService = requireNonNull(fareService, "fareService cannot be null");
        this.airportService = requireNonNull(airportService, "airportService cannot be null");
    }

    /**
     * Retrieves Fare offer for travels between airports
     *
     * @param originCode origin airport's code
     * @param destinationCode destination airport's code
     * @param currency offer's currency
     * @return
     */
    @GetMapping("/{originCode}/{destinationCode}")
    public DeferredResult<ResponseEntity<?>> getFares(@PathVariable String originCode, @PathVariable String destinationCode,
                                                          @RequestParam Optional<String> currency) {

        logger.debug("getFares(originCode: {}, destinationCode: {}, currency: {})", originCode, destinationCode, currency);

        DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
        result.onTimeout(() -> {
            logger.warn("Timeout while retrieving fare for origin {} and destination {}", originCode, destinationCode);
            result.setResult(status(GATEWAY_TIMEOUT).build());
        });

        fareService.getFareWithAirports(originCode, destinationCode, currency)
                .thenAccept((fareWithAirports) -> {
                    logger.debug("then accept (fareWithAirports: {})", fareWithAirports);

                    ResponseEntity<?> response = fareWithAirports
                            .<ResponseEntity<?>> map(f ->  ok(f))
                            .orElseGet(() -> notFound().build());
                    result.setResult(response);
                })
                .exceptionally(throwable -> {
                    // TODO: implement proper error handling
                    logger.error("Unexpected error while retrieve route's fare", throwable);
                    result.setResult(status(INTERNAL_SERVER_ERROR).build());
                    return null;
                });

        return result;
    }

}
