package com.afkl.cases.df;


import ch.qos.logback.classic.helpers.MDCInsertingServletFilter;
import com.codahale.metrics.MetricRegistry;
import de.codecentric.boot.admin.config.EnableAdminServer;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.actuate.metrics.dropwizard.DropwizardMetricServices;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import static java.util.Arrays.asList;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Configuration
@EnableAdminServer
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/airports").setViewName("airports");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // allow CORS from any origin so it is possible to host FE
        // in different environment
        registry.addMapping("/api/rest/**")
                .allowedOrigins("*");
    }

    @Bean
    public DropwizardMetricServices dropwizardMetricServices(MetricRegistry registry) {
        return new DropwizardMetricServices(registry);
    }

    @Bean
    public RequestIdMDCInsertingServletFilter requestIdMDCInsertingServletFilter() {
        return new RequestIdMDCInsertingServletFilter();
    }

    @Bean
    @Order(HIGHEST_PRECEDENCE)
    public FilterRegistrationBean gaugeFilterRegistrationBean(GaugeService gaugeService) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new TimerMetricsFilter(gaugeService));
        registrationBean.setUrlPatterns(asList("/api/rest/*"));
        return registrationBean;
    };

    @Bean
    public MDCInsertingServletFilter mdcInsertingServletFilter() {
        return new MDCInsertingServletFilter();
    }

}
