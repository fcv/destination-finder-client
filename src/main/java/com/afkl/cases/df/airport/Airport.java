package com.afkl.cases.df.airport;

import org.springframework.hateoas.core.Relation;

import java.io.Serializable;

@Relation(collectionRelation = "airports")
public class Airport implements Serializable {

    private static final long serialVersionUID = 6990034784785270319L;

    private String code;
    private String name;
    private String description;
    private Coordinates coordinates;
    private Airport parent;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Airport getParent() {
        return parent;
    }

    public void setParent(Airport parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Airport{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", coordinates=").append(coordinates);
        sb.append(", parent=").append(parent);
        sb.append('}');
        return sb.toString();
    }
}
