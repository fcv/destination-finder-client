package com.afkl.cases.df;

import org.slf4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.http.OAuth2ErrorHandler;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
@EnableOAuth2Client
public class RemoteApiConfiguration {

    private final Logger logger;

    public RemoteApiConfiguration(Logger logger) {
        this.logger = logger;
    }

    @Bean
    public RestTemplate restTemplate(ClientCredentialsResourceDetails resource) {

        logger.debug("restTemplate(resource: {})", resource);
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource);

        // configures a custom error handler which does not lead to an exception when a 404 is returned
        // and let client code analyse response's status code
        OAuth2ErrorHandler errorHandler = new OAuth2ErrorHandler(new DefaultResponseErrorHandler(), resource) {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                HttpStatus statusCode = response.getStatusCode();
                return !statusCode.equals(HttpStatus.NOT_FOUND) && super.hasError(response);
            }
        };

        restTemplate.setErrorHandler(errorHandler);
        return restTemplate;
    }

    @Bean
    @ConfigurationProperties("security.oauth2.client")
    public ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
        return new ClientCredentialsResourceDetails();
    }

    @Bean
    @ConfigurationProperties(prefix = "destination-finder.thread-pool")
    public ThreadPoolExecutorFactoryBean ThreadPoolTaskExecutor() {
        // Note: using a custom ThreadPoolExecutorFactoryBean implementation to propagate MDC values
        // I'm pretty sure there is a built-in spring functionality for that but I could not remember
        // how to enable it
        ThreadPoolExecutorFactoryBean factory = new MDCAwareThreadPoolExecutorFactoryBean();
        return factory;
    }

}
