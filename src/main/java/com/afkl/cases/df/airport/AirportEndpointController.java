package com.afkl.cases.df.airport;

import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@RestController
@RequestMapping(value = "/api/rest/v1/airports", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AirportEndpointController {

    private final Logger logger;
    private final AirportService airportService;
    private final AirportResourceAssembler airportAssembler;

    public AirportEndpointController(Logger logger, AirportService airportService, AirportResourceAssembler airportAssembler) {
        this.logger = requireNonNull(logger, "logger cannot be null");
        this.airportService = requireNonNull(airportService, "airportService cannot be null");
        this.airportAssembler = requireNonNull(airportAssembler, "airportAssembler cannot be null");
    }

    /**
     * List airports based on parameters:
     *
     * @param size the size of the result
     * @param page the page to be selected in the paged response
     * @param lang the language, supported ones are nl and en
     * @param term A search term that searches through code, name and description.
     *
     * @return
     */
    @GetMapping
    public PagedResources<Resource<Airport>> getAirports(@RequestParam Optional<Integer> size,
                                                         @RequestParam Optional<Integer> page,
                                                         @RequestParam Optional<String> lang,
                                                         @RequestParam Optional<String> term,
                                                         PagedResourcesAssembler<Airport> pageAssembler) {


        logger.debug("getAirports(size: {}, page: {}, lang: {}, term: {})", size, page, lang, term);
        Page<Airport> airportPage = airportService.listAirports(size, page, lang, term);
        return pageAssembler.toResource(airportPage, airportAssembler);
    }

    /**
     * Retrieve a single airport based on its {@code code}.
     * @param code airport's code
     * @return
     */
    @GetMapping(value = "/{code}")
    public ResponseEntity<?> getAirport(@PathVariable String code) {
        logger.debug("getAirport(code: {})", code);
        Optional<Airport> airport = airportService.getAirportByCode(code);
        return airport.<ResponseEntity<?>>map(ap -> ResponseEntity.ok(ap))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
