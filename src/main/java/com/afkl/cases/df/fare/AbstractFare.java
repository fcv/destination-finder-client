package com.afkl.cases.df.fare;

import java.math.BigDecimal;

public abstract class AbstractFare {

    private BigDecimal amount;
    private String currency;

    public AbstractFare() {
    }

    public AbstractFare(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
