package com.afkl.cases.df;

import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Custom {@link ThreadPoolExecutorFactoryBean} whose created {@link ThreadPoolExecutor} propagate {@link MDC} context
 * to its task before actually execute them.
 *
 */
public class MDCAwareThreadPoolExecutorFactoryBean extends ThreadPoolExecutorFactoryBean {

    @Override
    protected ThreadPoolExecutor createExecutor(int corePoolSize, int maxPoolSize, int keepAliveSeconds, BlockingQueue<Runnable> queue, ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
        return new MDCAwareThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveSeconds, queue, threadFactory, rejectedExecutionHandler);
    }

    private static class MDCAwareThreadPoolExecutor extends ThreadPoolExecutor {

        public MDCAwareThreadPoolExecutor(int corePoolSize, int maxPoolSize, int keepAliveSeconds, BlockingQueue<Runnable> queue, ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
            super(corePoolSize, maxPoolSize, keepAliveSeconds, TimeUnit.SECONDS, queue, threadFactory, rejectedExecutionHandler);
        }

        @Override
        public void execute(Runnable runnable) {
            super.execute(propagatingMDC(runnable));
        }

        protected Runnable propagatingMDC(Runnable runnable) {
            final Map<String, String> outerContextMap = MDC.getCopyOfContextMap();

            Runnable decorated = () -> {
                Map<String, String> originalContextMap = MDC.getCopyOfContextMap();
                try {
                    setContextMapOrClear(outerContextMap);
                    runnable.run();
                } finally {
                    setContextMapOrClear(originalContextMap);
                }
            };
            return decorated;
        }

        /**
         * Set contextMap to MDC or clear it if contextMap is null
         *
         * @param contextMap
         */
        private static void setContextMapOrClear(Map<String, String> contextMap) {
            if (contextMap != null) {
                MDC.setContextMap(contextMap);
            } else {
                MDC.clear();
            }
        }
    }


}
